import {createApp} from 'vue';
import App from './App.vue';
import router from './router';
import axios from 'axios';
import {VueToastr} from 'vue-toastr';
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap'
import 'vue-toastr/dist/style.css';
import vueCountryRegionSelect from 'vue3-ts-country-region-select';
import CountryFlag from 'vue-country-flag-next';

const app = createApp(App);
axios.defaults.headers.common['x-api-version'] = '1.0';
app.config.globalProperties.$http = axios;
app.config.globalProperties.$hitmapsDomain = 'https://api.hitmaps.com';
app.config.globalProperties.$rouletteApiDomain = 'https://rouletteapi.hitmaps.com';

let apiDomain = '';
let domain = '';
switch (window.location.hostname) {
    case 'localhost':
        apiDomain = 'https://localhost:7142';
        domain = 'http://localhost:3000';
        break;
    case 'testtournaments.hitmaps.com':
        apiDomain = 'https://testtournamentsapi.hitmaps.com';
        domain = `${document.location.protocol}//${window.location.hostname}`;
        break;
    case 'tournaments.hitmaps.com':
    default:
        apiDomain = 'https://tournamentsapi.hitmaps.com';
        domain = `${document.location.protocol}//${window.location.hostname}`;
        break;
}
app.config.globalProperties.$apiDomain = apiDomain;
app.config.globalProperties.$domain = domain;
app.provide('apiDomain', apiDomain);
app.provide('domain', domain);

const port = document.location.port ? `:${document.location.port}` : '';
app.config.globalProperties.$vueDomain = `${document.location.protocol}//${window.location.hostname}${port}`;
app.provide('vueDomain', `${document.location.protocol}//${window.location.hostname}${port}`);

app.use(VueToastr, {
    defaultPosition: 'toast-top-left',
    defaultTimeout: 3000,
    defaultProgressBar: true
});
app.use(vueCountryRegionSelect);
app.component('country-flag', CountryFlag);
app.use(router);

app.mount('#app');
