import TimeZone from '@/components/TimeZone';
import moment from 'moment-timezone';

export default class Utils {
    public static decodeJWT(token: string|null): object|null {
        if (token === null) {
            return null;
        }

        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        const jsonPayload = decodeURIComponent(atob(base64).split('').map((c) => {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));

        return JSON.parse(jsonPayload);
    }

    public static getTimeZones(): TimeZone[] {
        return moment.tz.names().map((name: string) => {
            const tz = moment.tz.zone(name)!;

            return new TimeZone(tz.name, tz.utcOffset(new Date().getTime()));
        });
    }

    public static guessUserTimeZone(): string {
        return moment.tz.guess();
    }

    public static changeTimeZone(date: Date, timeZone: string): Date {
        return new Date(
            date.toLocaleString('en-US', {
                timeZone
            })
        );
    }

    public static getDiscordHeader(cookies: any): any {
        if (!cookies.isKey('discord-token-type') || !cookies.isKey('discord-access-token')) {
            return null;
        }

        return {
            headers: {
                'X-Discord-Token': `${cookies.get('discord-token-type')} ${cookies.get('discord-access-token')}`
            }
        };
    }

    public static beginDiscordLogin(cookies: any, domain: string): void {
        cookies.set('redirect-to', `${window.location.pathname}${window.location.search}`, '600s');
        window.location.href = `https://discordapp.com/api/oauth2/authorize?client_id=681919936469401687&redirect_uri=${encodeURIComponent(domain)}/auth&response_type=token&scope=connections%20identify%20guilds`;
    }

    public static formatDateTime(date: Date|string,
                                 format: string,
                                 timezone: string = Intl.DateTimeFormat().resolvedOptions().timeZone,
                                 relativeFormat: string = ''): string {
        const momentDate = moment(date).tz(timezone);
        return relativeFormat !== '' ?
            `${momentDate.format(relativeFormat)} (${momentDate.fromNow()})` :
            momentDate.format(format);
    }

    public static getCdnFriendlyUrl(url: string): string {
        return url;
    }

    public static copyToClipboard(content: string): Promise<any> {
        return navigator.clipboard.writeText(content);
    }
}
