export default class DiscordConnection {
    public id!: string;
    public name!: string;
    public revoked?: boolean;
    public type!: string;

    constructor(init?: Partial<DiscordConnection>) {
        Object.assign(this, init);
    }
}
