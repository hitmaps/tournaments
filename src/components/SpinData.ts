import Timer from "@/components/Timer";

export default class SpinData {
    public matchComplete: boolean = false;
    public pointsForVictory?: number;
    public matchDuration?: number = 30;
    public matchTime: Date = new Date();
    public showSpin: boolean = false;
    public showTimer: boolean = false;
    public timer: Timer = new Timer();
}
