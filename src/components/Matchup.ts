import Participant from '@/components/Participant';
import MapChoice from '@/components/MapChoice';

export default class Matchup {
    public matchId!: number;
    public round!: number;
    public group!: string;
    public winnersBracket!: boolean;
    public matchupTime!: string|null;
    public matchStatus!: string;
    public participants!: Participant[];
    public shoutcasterUrl!: string|null;
    public shoutcasterName!: string|null;
    public mapSelections!: MapChoice[];
    public pointsForWin!: number|null;


    constructor(init?: Partial<Matchup>) {
        Object.assign(this, init);
    }
}
