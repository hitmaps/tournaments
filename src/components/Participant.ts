export default class Participant {
    public competitorId!: number;
    public challongeId!: number;
    public challongeName!: string;
    public discordAvatarUrl!: string;
    public discordSnowflake!: string;
    public score!: number;

    constructor(init?: Partial<Participant>) {
        Object.assign(this, init);
    }
}
