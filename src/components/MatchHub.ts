import {HubConnection, HubConnectionBuilder, HubConnectionState, LogLevel} from '@microsoft/signalr';

export default class MatchHub {
    private connection!: HubConnection;
    private startedPromise!: Promise<void>;


    constructor(baseUrl: string) {
        this.connection = new HubConnectionBuilder()
            .withUrl(`${baseUrl}/ws/match-hub`)
            .withAutomaticReconnect()
            .configureLogging(LogLevel.Information)
            .build();
    }

    public start(): Promise<void> {
        const start = (): Promise<void> => {
            this.startedPromise = this.connection.start().catch(err => {
                console.error('Failed to connect to hub', err);
                return new Promise((res, rej) => {
                    setTimeout(() => start().then(res).catch(rej), 5000);
                });
            });
            return this.startedPromise;
        };
        this.connection.onclose(() => start());

        // noinspection JSIgnoredPromiseFromCall
        return start();
    }

    public stop(): Promise<void> {
        return this.connection.stop();
    }

    public state(): HubConnectionState {
        return this.connection.state;
    }

    public onReconnecting(callback: (error: Error|undefined) => void): void {
        this.connection.onreconnecting(callback);
    }

    public onReconnected(callback: (connectionId: string|undefined) => void): void {
        this.connection.onreconnected(callback);
    }

    public onClose(callback: (error: Error|undefined) => void): void {
        this.connection.onclose(callback);
    }

    public on(event: string, callback: (args: any[]) => void): void {
        this.connection.on(event, callback);
    }

    public off(event: string): void {
        this.connection.off(event);
    }

    public joinMatch(matchId: number): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('JoinMatchGroup', {
                matchId: matchId
            })).catch(console.error);
    }

    //region Admin Actions
    public adminMatchInfo(matchId: number) {
        return this.startedPromise
            .then(() => this.connection.invoke('AdminMatchInfo', {
                matchId: matchId
            })).catch(console.error);
    }

    public adminJoinMatch(matchId: number): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('AdminJoinMatchGroup', {
                matchId: matchId
            }))
            .catch(console.error);
    }

    public adminLeaveMatch(matchId: number): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('AdminLeaveMatchGroup', {
                matchId: matchId,
            })).catch(console.error);
    }

    public adminJoinMessageAck(matchId: number, discordToken: string): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('AdminJoinMessageAck', {
                matchId: matchId,
                discordToken: discordToken
            })).catch(console.error);
    }

    public adminLeaveMessageAck(matchId: number): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('AdminJoinMessageAck', {
                matchId: matchId
            })).catch(console.error);
    }

    public joinGlobalScoreUpdate(): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('JoinGlobalScoreUpdate')).catch(console.error);
    }

    public leaveGlobalScoreUpdate(): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('LeaveGlobalScoreUpdate')).catch(console.error);
    }

    public adminClearSpin(matchId: number, discordToken: string) {
        return this.startedPromise
            .then(() => this.connection.invoke('ClearSpin', {
                matchId: matchId,
                discordToken: discordToken
            })).catch(console.error);
    }

    public adminSendSpin(matchId: number, discordToken: string, spinData: any) {
        return this.startedPromise
            .then(() => this.connection.invoke('SendSpin', {
                matchId: matchId,
                discordToken: discordToken,
                ...spinData
            })).catch(console.error);
    }

    public adminProcessCompletion(matchId: number, discordToken: string, completionData: any) {
        return this.startedPromise
            .then(() => this.connection.invoke('ProcessCompletion', {
                matchId: matchId,
                discordToken: discordToken,
                ...completionData
            })).catch(console.error);
    }

    public adminRemoveLastResult(matchId: number, discordToken: string, mapSelectionId: number) {
        return this.startedPromise
            .then(() => this.connection.invoke('RemoveLastResult', {
                matchId: matchId,
                discordToken: discordToken,
                mapSelectionId: mapSelectionId
            })).catch(console.error);
    }

    public adminForfeitPlayer(matchId: number, discordToken: string, competitorId: number, forfeit: boolean) {
        return this.startedPromise
            .then(() => this.connection.invoke('ForfeitPlayer', {
                matchId: matchId,
                discordToken: discordToken,
                competitorId: competitorId,
                forfeit: forfeit
            })).catch(console.error);
    }

    public adminSendMessage(matchId: number, discordToken: string, competitorId: number, message: string) {
        return this.startedPromise
            .then(() => this.connection.invoke('AdminSendMessage', {
                matchId: matchId,
                discordToken: discordToken,
                competitorId: competitorId,
                message: message
            })).catch(console.error);
    }

    public adminUpdateObjectives(matchId: number, discordToken: string, objectives: object) {
        return this.startedPromise
            .then(() => this.connection.invoke('AdminUpdateObjectives', {
                matchId: matchId,
                discordToken: discordToken,
                objectives: objectives
            })).catch(console.error);
    }

    public adminFinalizeMatch(matchId: number, discordToken: string) {
        return this.startedPromise
            .then(() => this.connection.invoke('AdminFinalizeMatch', {
                matchId: matchId,
                discordToken: discordToken
            })).catch(console.error);
    }
    //endregion

    public loadObjectives(matchId: number) {
        return this.startedPromise
            .then(() => this.connection.invoke('LoadObjectives', {
                matchId: matchId
            })).catch(console.error);
    }

    //region Competitor Actions
    public leaveMatch(matchId: number): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('LeaveMatchGroup', {
                matchId: matchId
            })).catch(console.error);
    }

    public competitorJoinMessage(matchId: number, competitorPublicId: string): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('CompetitorJoinMessageGroup', {
                matchId: matchId,
                competitorPublicId: competitorPublicId
            })).catch(console.error);
    }

    public competitorLeaveMessage(matchId: number, competitorId: number): Promise<void> {
        return this.startedPromise
            .then(() => this.connection.invoke('ParticipantLeaveMessageGroup', {
                matchId: matchId,
                competitorId: competitorId
            })).catch(console.error);
    }

    public competitorMatchInfo(matchId: number, publicId: string) {
        return this.startedPromise
            .then(() => this.connection.invoke('CompetitorMatchInfo', {
                matchId: matchId,
                publicId: publicId
            })).catch(console.error);
    }

    public competitorCheckMessages(matchId: number, publicId: string) {
        return this.startedPromise
            .then(() => this.connection.invoke('CompetitorCheckMessages', {
                matchId: matchId,
                publicId: publicId
            })).catch(console.error);
    }

    public competitorUpdateMessageState(matchId: number, publicId: string, state: string) {
        return this.startedPromise
            .then(() => this.connection.invoke('CompetitorUpdateMessageState', {
                matchId: matchId,
                publicId: publicId,
                state: state
            })).catch(console.error);
    }

    public competitorNotifyCompletion(matchId: number, publicId: string, timerFingerprint: string) {
        return this.startedPromise
            .then(() => this.connection.invoke('CompetitorNotifyCompletion', {
                matchId: matchId,
                publicId: publicId,
                timerFingerprint: timerFingerprint
            })).catch(console.error);
    }
    //endregion

    public sendHeartbeat(matchId: number, publicId: string, timerFingerprint: string): Promise<any> {
        return this.startedPromise
            .then(() => this.connection.invoke<any>('SendHeartbeat', {
                matchId: matchId,
                publicId: publicId,
                timerFingerprint: timerFingerprint
            })).catch(console.error);
    }
}
