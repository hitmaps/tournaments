export default class Stage {
    public id!: number;
    public name!: string;
    public type!: string;
    public state!: string;
    public settings!: object;

    constructor(init?: Partial<Stage>) {
        Object.assign(this, init);
    }
}