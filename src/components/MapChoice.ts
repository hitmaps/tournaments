import HitmapsMapInfo from '@/components/HitmapsMapInfo';

export default class MapChoice {
    public competitorId!: number;
    public id!: number;
    public mapSlug!: string;
    public selectionType!: string;
    public mapInfo!: HitmapsMapInfo;

    constructor(init?: Partial<MapChoice>) {
        Object.assign(this, init);
    }
}
