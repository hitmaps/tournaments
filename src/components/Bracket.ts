import Stage from "@/components/Stage";

export default class Bracket {
    public id!: number;
    public challongeName?: string;
    public participantLimit!: number;
    public platform!: string;
    public platformIcons!: string;
    public currentParticipantCount!: number;
    public stages!: Stage[];

    constructor(init?: Partial<Bracket>) {
        Object.assign(this, init);
    }
}