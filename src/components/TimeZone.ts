export default class TimeZone {
    public name!: string;
    public currentOffsetMinutes!: number;
    public displayOffsetHours!: string;

    constructor(name: string, currentOffsetMinutes: number) {
        this.name = name;
        this.currentOffsetMinutes = currentOffsetMinutes;
        this.displayOffsetHours = (-1 * this.currentOffsetMinutes / 60).toFixed(1);
    }
}
