export default class PlayerStatus {
    public text: string = 'Offline';
    public color: string = 'red';
}