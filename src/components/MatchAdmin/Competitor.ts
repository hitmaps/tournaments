import PlayerStatus from "@/components/MatchAdmin/PlayerStatus";

export default class Competitor {
    public id!: number;
    public discordId!: string;
    public avatarUrl!: string;
    public name!: string;
    public countryCode!: string;
    public status: PlayerStatus = new PlayerStatus();
    public lastPing?: string|null;
    public completeTime?: string|null;
    public forfeit!: boolean;
    public publicId!: string;

    constructor(init?: Partial<Competitor>) {
        Object.assign(this, init);
    }
}