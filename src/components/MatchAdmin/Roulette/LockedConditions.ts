export default class LockedConditions {
    public lockedTargetConditions: any[] = [];
    public lockedAdditionalObjectives: any[] = [];
    public lockMission: boolean = false;

    constructor(init?: Partial<LockedConditions>) {
        Object.assign(this, init);
    }

    public isEmpty(): boolean {
        return !(this.lockedTargetConditions.length || this.lockedAdditionalObjectives.length);
    }

    public isNotEmpty(): boolean {
        return !this.isEmpty();
    }

    public getAdditionalObjectivesForApiRequest(): any[] {
        return this.lockedAdditionalObjectives.map(x => {
            return {
                objective: x.target,
                completionMethod: x.killMethod,
                disguise: x.disguise
            };
        });
    }
}