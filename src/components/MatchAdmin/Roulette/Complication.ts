export default class Complication {
    public name!: string;
    public description!: string;
    public tileUrl!: string;

    constructor(init?: Partial<Complication>) {
        Object.assign(this, init);
    }
}