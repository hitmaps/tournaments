import Condition from "@/components/MatchAdmin/Roulette/Condition";
import Complication from "@/components/MatchAdmin/Roulette/Complication";


export default class SpinResult {
    public type: string = 'KILL';
    public target: any;
    public killMethod: Condition = new Condition();
    public disguise: Condition = new Condition();
    public complications: Complication[] = [];

    constructor(init?: Partial<SpinResult>) {
        Object.assign(this, init);
    }

    public convertToAdditionalObjectiveForApi(): any {
        return {
            type: this.type,
            objective: this.target,
            completionMethod: this.killMethod,
            disguise: this.disguise
        };
    }
}
