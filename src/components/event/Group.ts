export default class Group {
    public number!: number;
    public members: object[] = [];

    constructor(init?: Partial<Group>) {
        Object.assign(this, init);
    }
}