export default class MapColor {
    public static getColorForMission(mission: string) {
        const missionToColor: any = {
            'The Showstopper': '#f5695e',
            'World of Tomorrow': '#9fbef1',
            'A Gilded Cage': '#f7d05d',
            'Club 27': '#daa0bc',
            'Freedom Fighters': '#c5a28a',
            'Situs Inversus': '#98ccc8',
            'The Finish Line': '#f569be',
            'Three-Headed Serpent': '#4db81f',
            'Chasing a Ghost': '#bda8f3',
            'Another Life': '#f8b04f',
            'The Ark Society': '#9e9b9b',
            'Golden Handshake': '#c7a334',
            'The Last Resort': '#53dfce',
            'On Top Of The World': '#df9549',
            'Death In The Family': '#7c93bb',
            'Apex Predator': '#9cb893',
            'End Of An Era': '#df5edf',
            'The Farewell': '#c3586a'
        };

        if (missionToColor[mission]) {
            return missionToColor[mission];
        }

        return 'lime';
    }
}