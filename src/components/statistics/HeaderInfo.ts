export default class HeaderInfo {
    public heading!: string;
    public description!: string;

    constructor(heading: string, description: string) {
        this.heading = heading;
        this.description = description;
    }

    public static UNKNOWN = new HeaderInfo('Unknown', 'Unknown');
}