export default class Event {
    public id!: number;
    public name!: string;
    public tileUrl!: string;
    public eventType!: string;
    public slug!: string;
    public isoRegistrationStartDate!: string;
    public isoRegistrationEndDate!: string;
    public isoEventEndDate!: string;
    public rules!: string;
    public formattedRules!: string;
    public guildId!: number;
    public adminRoles!: number[];
    public overlayBackgroundUrl!: string;
    public usesChallonge!: boolean;

    constructor(init?: Partial<Event>) {
        Object.assign(this, init);
    }
}
