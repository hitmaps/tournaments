export default class Timer {
    public fingerprint!: string;
    public paused: boolean = false;
    public matchDurationInMinutes?: number;
    public startTime?: string;
    public endTime?: string;
    public serverTime!: string;

    constructor(init?: Partial<Timer>) {
        Object.assign(this, init);
    }
}
