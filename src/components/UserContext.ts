import DiscordConnection from '@/components/DiscordConnection';

export default class UserContext {
    public name!: string;
    public discriminator!: string;
    public serverMember!: boolean;
    public snowflake!: number;
    public connections: DiscordConnection[] = [];
    public serverRoles: string[] = [];
    public tourneyBanned!: boolean;

    constructor(init?: Partial<UserContext>) {
        Object.assign(this, init);
    }
}
