interface AssociativeArray {
    [key: string]: string
}

export default class Locations {
    private static locationNamesToAbbreviation: AssociativeArray = {
        'paris': 'PAR',
        'sapienza': 'SAP',
        'marrakesh': 'MAR',
        'bangkok': 'BKK',
        'colorado': 'COL',
        'hokkaido': 'HOK',
        'miami': 'MIA',
        'santa-fortuna': 'SF',
        'mumbai': 'MUM',
        'whittleton-creek': 'WC',
        'isle-of-sgail': 'SGL',
        'new-york': 'NY',
        'haven-island': 'HVN',
        'dubai': 'DUB',
        'dartmoor': 'DAR',
        'berlin': 'BER',
        'chongqing': 'CKG',
        'mendoza': 'MDZ',
        'ambrose-island': 'AMB'
    };

    private static locationNamesToCountryFlag: AssociativeArray = {
        'paris': 'fr',
        'sapienza': 'it',
        'marrakesh': 'ma',
        'bangkok': 'th',
        'colorado': 'us',
        'hokkaido': 'jp',
        'miami': 'us',
        'santa-fortuna': 'co',
        'mumbai': 'in',
        'whittleton-creek': 'us',
        'isle-of-sgail': 'gb',
        'new-york': 'us',
        'haven-island': 'mv',
        'dubai': 'ae',
        'dartmoor': 'gb',
        'berlin': 'de',
        'chongqing': 'cn',
        'mendoza': 'ar',
        'ambrose-island': 'id'
    };

    public static getAbbreviation(location: string): string {
        return this.locationNamesToAbbreviation[location];
    }

    public static getCountryCode(location: string): string {
        return this.locationNamesToCountryFlag[location];
    }
}