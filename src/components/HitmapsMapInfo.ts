export default class HitmapsMapInfo {
    public game!: string;
    public hitmapsSlug!: string;
    public id!: number;
    public location!: string;
    public name!: string;

    constructor(init?: Partial<HitmapsMapInfo>) {
        Object.assign(this, init);
    }
}
