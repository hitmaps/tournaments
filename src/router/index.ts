import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'Home',
            component: () => import('../views/Home.vue'),
            meta: {
                title: 'Home'
            }
        },
        {
            path: '/events/:eventSlug',
            name: 'Event',
            component: () => import('../views/Event.vue'),
            meta: {
                title: 'Event'
            },
            props: true
        },
        {
            path: '/events/:eventSlug/register',
            name: 'Register',
            component: () => import('../views/Register.vue'),
            meta: {
                title: 'Event'
            },
            props: true
        },
        {
            path: '/events/:eventSlug/players',
            name: 'PlayerManager',
            meta: {
                title: 'Player Manager'
            },
            component: () => import('../views/PlayerManager.vue'),
            props: true
        },
        {
            path: '/events/:eventSlug/matches/:matchId',
            name: 'MatchHome',
            meta: {
                title: 'Match'
            },
            component: () => import('../views/MatchAdmin/MatchHome.vue'),
            props: route => {
                return {
                    eventSlug: route.params.eventSlug,
                    matchId: parseInt(route.params.matchId as string)
                }
            }
        },
        {
            path: '/events/rrwc-2024/match-overlay/:matchId',
            name: 'Rrwc2024Overlay',
            meta: {
                title: 'Match'
            },
            component: () => import('../views/MatchOverlay/Rrwc2024Overlay.vue'),
            props: route => {
                return {
                    eventSlug: route.params.eventSlug,
                    matchId: parseInt(route.params.matchId as string)
                }
            }
        },
        {
            path: '/events/rr16/match-overlay/:matchId',
            name: 'Rr16Overlay',
            meta: {
                title: 'Match'
            },
            component: () => import('../views/MatchOverlay/Rr16Overlay.vue'),
            props: route => {
                return {
                    eventSlug: route.params.eventSlug,
                    matchId: parseInt(route.params.matchId as string)
                }
            }
        },
        {
            path: '/events/:eventSlug/matches/:matchId/:publicId',
            name: 'CompetitorHome',
            meta: {
                title: 'Competitor Home'
            },
            component: () => import('../views/MatchAdmin/CompetitorHome.vue'),
            props: route => {
                return {
                    eventSlug: route.params.eventSlug,
                    matchId: parseInt(route.params.matchId as string),
                    publicId: route.params.publicId
                }
            }
        },
        {
            path: '/auth',
            name: 'Auth',
            component: () => import('../views/DiscordAuth.vue'),
            meta: {
                title: 'Please Wait...'
            }
        },
        {
            path: '/events/:eventSlug/statistics',
            name: 'Statistics',
            component: () => import('../views/Statistics.vue'),
            meta: {
                title: 'Statistics'
            },
            props: true
        },
        {
            path: '/upcoming-matches/:eventSlug',
            name: 'UpcomingMatchesOverlay',
            component: () => import('../views/UpcomingMatchesOverlay.vue'),
            meta: {
                title: 'Upcoming Matches'
            },
            props: true
        },
        // TODO Retire group-standings and brackets
        {
            path: '/group-standings/:eventSlug',
            name: 'GroupStandingsOverlay',
            component: () => import('../views/StandingsOverlay.vue'),
            meta: {
                title: 'Standings'
            },
            props: true
        },
        {
            path: '/standings/:eventSlug',
            name: 'StandingsOverlay',
            component: () => import('../views/StandingsOverlay.vue'),
            meta: {
                title: 'Standings'
            },
            props: true
        },
        {
            path: '/brackets/:eventSlug',
            name: 'BracketOverlay',
            component: () => import('../views/StandingsOverlay.vue'),
            meta: {
                title: 'Brackets'
            },
            props: true
        }
    ]
});

export default router;

router.beforeEach((to, from, next) => {
    const title = to.meta.title;

    if (title) {
        document.title = `${title} | HITMAPS™ Tournaments`;
    }

    next();
})
